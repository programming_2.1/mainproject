package be.kdg.java2.repository;

import be.kdg.java2.domain.Airplane;
import be.kdg.java2.domain.Hangar;
import be.kdg.java2.domain.Pilot;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataFactory {
    public static List<Airplane> airplanes = new ArrayList<>();
    public static List<Pilot> pilots = new ArrayList<>();

    public static void seed() {

        Airplane A300 = new Airplane("A300", Airplane.airplaneType.passenger, true, LocalDate.of(2018, 7, 24), 41505.5d);
        Airplane BAE_146 = new Airplane("BAE 146", Airplane.airplaneType.passenger, true, LocalDate.of(2021, 8, 22), 61505d);
        Airplane Boeing_707 = new Airplane("Boeing 707", Airplane.airplaneType.business, true, LocalDate.of(2021, 4, 20), 50034d);
        Airplane Saab_340 = new Airplane("Saab 340", Airplane.airplaneType.military, true, LocalDate.of(2021, 2, 27), 35505d);

        Pilot EadieJohn = new Pilot("Eadie John", 28, 76, false, LocalDate.of(2021, 8, 22));
        Pilot JaneWainwright = new Pilot("Jane Wainwright", 32, 55, true, LocalDate.of(2018, 7, 24));
        Pilot JesseGriffin = new Pilot("Jesse Griffin", 42, 84, false, LocalDate.of(2018, 7, 24));
        Pilot VincenzoMarkham = new Pilot("Vincenzo Markham", 35, 69, false, LocalDate.of(2021, 2, 27));

        Hangar firstHangar = new Hangar(0, 20, false, 2500.85d, LocalDate.of(2021, 11, 20));
        Hangar secondHangar = new Hangar(1, 15, true, 2340.50d, LocalDate.of(2021, 12, 8));

        A300.addPilots(JaneWainwright, JesseGriffin);
        BAE_146.addPilots(EadieJohn, JaneWainwright);
        Boeing_707.addPilots(VincenzoMarkham, JesseGriffin, JaneWainwright);
        Saab_340.addPilots(VincenzoMarkham);

        EadieJohn.addAirplanes(BAE_146);
        JaneWainwright.addAirplanes(A300, BAE_146, Boeing_707);
        JesseGriffin.addAirplanes(A300, Boeing_707);
        VincenzoMarkham.addAirplanes(Boeing_707, Saab_340);

        A300.setHangar(firstHangar);
        BAE_146.setHangar(firstHangar);
        Boeing_707.setHangar(firstHangar);
        Saab_340.setHangar(secondHangar);

        firstHangar.addAirplanes(A300, BAE_146, Boeing_707);
        secondHangar.addAirplanes(Saab_340);

        airplanes.addAll(Arrays.asList(A300, BAE_146, Boeing_707, Saab_340));
        pilots.addAll(Arrays.asList(EadieJohn, JaneWainwright, JesseGriffin, VincenzoMarkham));
    }
}
