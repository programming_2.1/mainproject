package be.kdg.java2.repository;

import be.kdg.java2.domain.Airplane;
import be.kdg.java2.domain.Pilot;
import be.kdg.java2.util.LocalDateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class JSONSaver {
    private final String airplanesFileName = "airplanes.json";
    private final String pilotsFileName = "pilots.json";
    private final Gson gson;

    public JSONSaver() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
        gson = gsonBuilder.create();
    }

    public String getAirplanesFileName() {
        return airplanesFileName;
    }

    public String getPilotsFileName() {
        return pilotsFileName;
    }

    public void saveAirplanes(List<Airplane> airplanes) {
        String jsonString = gson.toJson(airplanes);
        try (FileWriter jsonWriter = new FileWriter(airplanesFileName)) {
            jsonWriter.write(gson.toJson(jsonString));
            System.out.println("Data is saved to airplanes.json...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePilots(List<Pilot> pilots) {
        String jsonString = gson.toJson(pilots);
        try (FileWriter jsonWriter = new FileWriter(pilotsFileName)) {
            jsonWriter.write(gson.toJson(jsonString));
            System.out.println("Data is saved to pilots.json...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
