package be.kdg.java2.services;

import be.kdg.java2.domain.Pilot;
import be.kdg.java2.repository.DataFactory;
import be.kdg.java2.repository.JSONSaver;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PilotService {
    private final JSONSaver jsonSaver = new JSONSaver();

    public List<Pilot> getAllPilots() { return DataFactory.pilots; }

    public void savePilotsToJSON(List<Pilot> pilots) { jsonSaver.savePilots(pilots); }

    public List<Pilot> getPilotsByNameAndAge(String name, String age) {
        Predicate<Pilot> filterCriteria;
        if (!name.isEmpty() && !age.isEmpty()) {
            filterCriteria = a -> a.getName().contains(name) && a.getAge() > Integer.parseInt(age);
        } else if (!name.isEmpty()) {
            filterCriteria = a -> a.getName().contains(name);
        } else {
            filterCriteria = a -> a.getAge() > Integer.parseInt(age);
        }

        return DataFactory.pilots
                .stream()
                .filter(filterCriteria)
                .collect(Collectors.toList());
    }
}
