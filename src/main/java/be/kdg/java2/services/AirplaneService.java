package be.kdg.java2.services;

import be.kdg.java2.domain.Airplane;
import be.kdg.java2.repository.DataFactory;
import be.kdg.java2.repository.JSONSaver;

import java.util.List;
import java.util.stream.Collectors;

public class AirplaneService {
    private final JSONSaver jsonSaver = new JSONSaver();

    public List<Airplane> getAllAirplanes() { return DataFactory.airplanes; }

    public void saveAirplanesToJSON(List<Airplane> airplanes) { jsonSaver.saveAirplanes(airplanes); }

    public List<Airplane> getAirplanesByType(String type) {
        return DataFactory.airplanes
                .stream()
                .filter(a -> a.getType().toString().equals(type))
                .collect(Collectors.toList());
    }
}
