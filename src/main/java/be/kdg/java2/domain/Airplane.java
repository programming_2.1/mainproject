package be.kdg.java2.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Airplane {
    private String name;
    private airplaneType type;
    private boolean foodService;
    private LocalDate lastFlight;
    private double weight;
    private Hangar hangar;
    private List<Pilot> pilots;

    public enum airplaneType {
        passenger, business, military;

        @Override
        public String toString() { return this.name().toLowerCase(); }
    }

    public Airplane(String name, airplaneType type, boolean foodService, LocalDate lastFlight, double weight) {
        this.name = name;
        this.type = type;
        this.foodService = foodService;
        this.lastFlight = lastFlight;
        this.weight = weight;
        this.pilots = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public airplaneType getType() {
        return type;
    }

    public boolean isFoodService() {
        return foodService;
    }

    public LocalDate getLastFlight() {
        return lastFlight;
    }

    public double getWeight() {
        return weight;
    }

    public Hangar getHangar() {
        return hangar;
    }

    public List<Pilot> getPilots() {
        return pilots;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(airplaneType type) {
        this.type = type;
    }

    public void setFoodService(boolean foodService) {
        this.foodService = foodService;
    }

    public void setLastFlight(LocalDate lastFlight) {
        this.lastFlight = lastFlight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHangar(Hangar hangar) {
        this.hangar = hangar;
    }

    public void setPilots(ArrayList<Pilot> pilots) {
        this.pilots = pilots;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", foodService=" + foodService +
                ", lastFlight=" + lastFlight +
                ", weight=" + weight +
                ", hangar=" + hangar.toString() +
                ", pilots=" + pilots.toString() +
                '}';
    }

    public void addPilots(Pilot... pilots) {
        this.pilots.addAll(Arrays.asList(pilots));
    }
}
