package be.kdg.java2.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Pilot {
    private String name;
    private int age;
    private double weight;
    private boolean isCopilot;
    private LocalDate lastCheckin;
    private transient List<Airplane> airplanes;

    public Pilot(String name, int age, double weight, boolean isCopilot, LocalDate lastCheckin) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.isCopilot = isCopilot;
        this.lastCheckin = lastCheckin;
        this.airplanes = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public LocalDate getLastCheckin() {
        return lastCheckin;
    }

    public boolean isCopilot() {
        return isCopilot;
    }

    public List<Airplane> getAirplanes() {
        return airplanes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setCopilot(boolean copilot) {
        isCopilot = copilot;
    }

    public void setLastCheckin(LocalDate lastCheckin) {
        this.lastCheckin = lastCheckin;
    }

    public void setAirplanes(ArrayList<Airplane> airplanes) {
        this.airplanes = airplanes;
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", isCopilot=" + isCopilot +
                ", lastCheckin=" + lastCheckin +
                '}';
    }

    public void addAirplanes(Airplane... airplanes) {
        this.airplanes.addAll(Arrays.asList(airplanes));
    }
}
