package be.kdg.java2.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hangar {
    private int id;
    private int capacity;
    private boolean isMilitary;
    private double length;
    private LocalDate nextCheckup;
    private transient List<Airplane> airplanes;

    public Hangar(int id, int capacity, boolean isMilitary, double length, LocalDate nextCheckup) {
        this.id = id;
        this.capacity = capacity;
        this.isMilitary = isMilitary;
        this.length = length;
        this.nextCheckup = nextCheckup;
        this.airplanes = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean isMilitary() {
        return isMilitary;
    }

    public double getLength() {
        return length;
    }

    public LocalDate getNextCheckup() {
        return nextCheckup;
    }

    public List<Airplane> getAirplanes() {
        return airplanes;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setMilitary(boolean military) {
        isMilitary = military;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setNextCheckup(LocalDate nextCheckup) {
        this.nextCheckup = nextCheckup;
    }

    public void setAirplanes(ArrayList<Airplane> airplanes) {
        this.airplanes = airplanes;
    }

    @Override
    public String toString() {
        return "Hangar{" +
                "id=" + id +
                ", capacity=" + capacity +
                ", isMilitary=" + isMilitary +
                ", length=" + length +
                ", nextCheckup=" + nextCheckup +
                '}';
    }

    public void addAirplanes(Airplane... airplanes) {
        this.airplanes.addAll(Arrays.asList(airplanes));
    }
}
