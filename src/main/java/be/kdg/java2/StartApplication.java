package be.kdg.java2;

import be.kdg.java2.domain.Airplane;
import be.kdg.java2.domain.Pilot;
import be.kdg.java2.repository.DataFactory;
import be.kdg.java2.services.AirplaneService;
import be.kdg.java2.services.PilotService;
import be.kdg.java2.util.LocalDateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StartApplication {
    private static final Scanner scanner = new Scanner(System.in);
    private static boolean gameOver = false;
    private static final AirplaneService airplaneService = new AirplaneService();
    private static final PilotService pilotService = new PilotService();

    public static void main(String[] args) {
        DataFactory.seed();
        do {
            showMenu();
        } while (!gameOver);
    }

    private static void showMenu() {
        System.out.println("What would you like to do?");
        System.out.println("==========================");
        System.out.println("0) Quit");
        System.out.println("1) Show all airplanes");
        System.out.println("2) Show airplanes of type");
        System.out.println("3) Show all pilots");
        System.out.println("4) Show pilots with name and/or older than a certain age");
        System.out.print("Choice (0-4): ");

        try {
            int choice = scanner.nextInt();
            scanner.nextLine();

            if (choice < 0 || choice > 4) {
                throw new Exception("Invalid number. Please try again.");
            }

            switch (choice) {
                case 0 -> gameOver = true;
                case 1 -> {
                    System.out.println("All airplanes");
                    System.out.println("==========================");
                    List<Airplane> airplanes = airplaneService.getAllAirplanes();
                    airplanes.forEach(System.out::println);
                    airplaneService.saveAirplanesToJSON(airplanes);
                }
                case 2 -> {
                    System.out.print("Type (1=passenger, 2=business, 3=military): ");
                    try {
                        int typeChoice = scanner.nextInt();
                        scanner.nextLine();

                        if (typeChoice < 0 || typeChoice > 3) {
                            throw new Exception("Invalid number. Please try again.");
                        }

                        switch (typeChoice) {
                            case 1 -> {
                                List<Airplane> filteredAirplanes = airplaneService.getAirplanesByType(Airplane.airplaneType.passenger.toString());
                                filteredAirplanes.forEach(System.out::println);
                                airplaneService.saveAirplanesToJSON(filteredAirplanes);
                            }
                            case 2 -> {
                                List<Airplane> filteredAirplanes = airplaneService.getAirplanesByType(Airplane.airplaneType.business.toString());
                                filteredAirplanes.forEach(System.out::println);
                                airplaneService.saveAirplanesToJSON(filteredAirplanes);

                            }
                            case 3 -> {
                                List<Airplane> filteredAirplanes = airplaneService.getAirplanesByType(Airplane.airplaneType.military.toString());
                                filteredAirplanes.forEach(System.out::println);
                                airplaneService.saveAirplanesToJSON(filteredAirplanes);
                            }
                        }

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 3 -> {
                    System.out.println("All pilots");
                    System.out.println("==========================");
                    List<Pilot> pilots = pilotService.getAllPilots();
                    pilots.forEach(System.out::println);
                    pilotService.savePilotsToJSON(pilots);
                }
                case 4 -> {
                    try {
                        System.out.print("Enter (part of) a name or leave blank: ");
                        String name = scanner.nextLine();

                        System.out.print("Enter an age to display pilots older than that or leave blank: ");
                        String age = scanner.nextLine();

                        List<Pilot> pilots = pilotService.getPilotsByNameAndAge(name, age);
                        pilots.forEach(System.out::println);
                        pilotService.savePilotsToJSON(pilots);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
